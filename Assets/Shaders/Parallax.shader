﻿Shader "Custom/Parallax (Diffuse)" 
{
	Properties
	{
		_MainTex("Texture", 2D) = "white" {}
		_UVMax("Max UV", Float) = 1.0
	}

	Subshader
	{
		Pass
		{
			Tags { "Queue" = "Opaque" "IgnoreProjector" = "True" "RenderType" = "Opaque" "PreviewType" = "Plane" }
			Lighting Off
			ZWrite Off

			CGPROGRAM

			#include "UnityCG.cginc"

			#pragma vertex vert
	      	#pragma fragment frag

	      	uniform sampler2D _MainTex;
	      	uniform float4 _MainTex_ST;
	      	uniform float _UVMax;

			struct VertexIn
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;
			};

			struct VertexOut
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
				float4 col : COLOR;
			};

			VertexOut vert(VertexIn v)
			{
				VertexOut o;

				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.col = v.col;

				return o;
			}

			float4 frag(VertexOut i) : COLOR
			{
				i.uv.y = min(i.uv.y, _UVMax);
	      		return tex2D(_MainTex, i.uv);
			}

			ENDCG
		}
	}
}

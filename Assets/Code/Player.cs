﻿using UnityEngine;
using System.Collections.Generic;

public sealed class Player : Entity 
{
	private HealthDisplay healthDisplay;

	private GameObject fireball;
	private Queue<EntityFireball> fireballs = new Queue<EntityFireball>(4);

	private bool enableFireball = false;

	public Vector2i TilePos { get; private set; }

	public override void Initialize(Map map, float x, float y, int ID)
	{
		base.Initialize(map, x, y, ID);

		healthDisplay = SceneItems.GetItem<HealthDisplay>("Health");
		immuneLength = 0.5f;

		map.BeginSimulating(this);
		fireball = Resources.Load<GameObject>("Entities/Fireball");
	}

	public void EnableFireball()
	{
		enableFireball = true;
	}

	private EntityFireball GetFireball()
	{
		EntityFireball next;

		if (fireballs.Count > 0)
			next = fireballs.Dequeue();
		else
		{
			next = Instantiate<GameObject>(fireball).GetComponent<EntityFireball>();
			next.Initialize(map, 0.0f, 0.0f, map.NextEntityID, fireballs);
		}
			
		next.MakeActive();
		return next;
	}

	public override void Simulate()
	{
		immuneRemaining = Mathf.Max(immuneRemaining - Time.deltaTime, 0.0f);

		bool grounded = controller.isGrounded;
		if (grounded) velocity.y = 0.0f;

		if (isDead) return;

		if (enableFireball && GameInput.DoFire())
		{
			Vector3 dir = rend.flipX ? Vector3.left : Vector3.right;
			dir = Vector3.Scale(dir, new Vector3(10.0f, 10.0f)).normalized;
			Vector3 origin = new Vector3(t.position.x + (dir.x * 0.5f), t.position.y + 0.2f);
			GetFireball().SetPath(origin, dir);
		}

		Vector2 accel = Vector2.zero;

		accel.x = GameInput.GetMove();

		if (GameInput.DoJump() && grounded)
			velocity.y = 15.0f;
		
		accel *= speed;
		accel += velocity * friction;
		accel.y = Gravity;

		Vector2 delta = (accel * 0.5f * Mathf.Pow(Time.deltaTime, 2.0f)) + (velocity * Time.deltaTime);
		velocity = (accel * Time.deltaTime) + velocity;

		if (!Mathf.Approximately(delta.x, 0.0f))
			rend.flipX = delta.x < 0.0f;

		if (!grounded) animationState = EntityState.Falling;
		else animationState = Mathf.Abs(delta.x) < 0.005f ? EntityState.Idle : EntityState.Walking;
		
		PlayAnimation();
		controller.Move(delta);

		TilePos = Utils.WorldToTilePos(Position);

		if (CheckEnvironment(TilePos)) 
		{
			if (Position.y < 0.0f)
				Kill(false);
			else map.ChangeActiveChunk(chunkPos);
		}
	}

	private void OnTriggerStay(Collider other)
	{
		if (other.gameObject.layer == CollisionLayer.Enemy)
		{
			Vector3 dir = Utils.GetHitDirection(other, t.position);

			if (dir.y >= 0.8f)
			{
				SetVelocityY(10.0f);
				other.GetComponentInParent<Entity>().TakeDamage(10, Vector3.zero);
			}
			else
			{
				if (Mathf.Approximately(dir.x, 0.0f))
					dir.x = Mathf.Sign(t.position.x - other.transform.position.x) * 0.5f;

				TakeDamage(2, dir);
			}
		}
	}

	private void PlayAnimation()
	{
		switch (animationState)
		{
		case EntityState.Idle:
		case EntityState.Falling:
			animator.Play(AnimationStates.Idle);
			break;

		case EntityState.Walking:
			animator.Play(AnimationStates.Walk);
			break;
		}
	}

	public override void TakeDamage(int damage, Vector3 dir)
	{
		base.TakeDamage(damage, dir);
		healthDisplay.Recompute(health);

		if (health <= 0) Kill(false);
	}

	protected override void Kill(bool addScore)
	{
		if (health > 0)
		{
			health = 0;
			healthDisplay.Recompute(health);
		}

		isDead = true;
		velocity = Vector2.zero;
		ToggleComponents(false);
		map.Invoke("GameOver", 3.0f);
	}

	public void OnSpawn()
	{
		fireballs.Clear();
	}
}

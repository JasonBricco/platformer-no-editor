﻿using UnityEngine;
using System.Collections;

public sealed class ScreenFader : MonoBehaviour 
{
	[SerializeField] private Material fadeMaterial;
	private Color fadeColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);

	public void SetFade(float r, float g, float b, float a)
	{
		fadeColor = new Color(r, g, b, a);
	}

	public void SetPause()
	{
		StopAllCoroutines();
		fadeColor.a = 0.7f;
	}

	public void RemovePause()
	{
		fadeColor.a = 0.0f;
	}

	public void GradualFade(float time, Color target, Method callback)
	{
		if (fadeColor != target)
			StartCoroutine(DoGradualFade(time, target, callback));
	}

	private IEnumerator DoGradualFade(float time, Color target, Method callback)
	{
		float mul = 1.0f / time;
		float t = 0.0f; 

		while (t < 1.0f)
		{
			fadeColor = Color.Lerp(fadeColor, target, t);
			t += (Time.unscaledDeltaTime * mul);
			yield return null;
		}

		fadeColor = target;

		if (callback != null)
			callback.Invoke();
	}

	private void OnPostRender()
	{
		if (fadeColor.a == 0.0f) return;

		fadeMaterial.SetColor("_Color", fadeColor);

		GL.PushMatrix();
		GL.LoadOrtho();

		fadeMaterial.SetPass(0);
		GL.Begin(GL.QUADS);

		GL.Vertex3(0.0f, 0.0f, 0.1f);
		GL.Vertex3(1.0f, 0.0f, 0.1f);
		GL.Vertex3(1.0f, 1.0f, 0.1f);
		GL.Vertex3(0.0f, 1.0f, 0.1f);

		GL.End();
		GL.PopMatrix(); 
	}
}
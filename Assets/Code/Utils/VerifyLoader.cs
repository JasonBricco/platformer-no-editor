﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class VerifyLoader : MonoBehaviour 
{
	private void Awake()
	{
		GameObject carrier = GameObject.FindWithTag("Carrier");
		if (carrier == null) SceneManager.LoadScene("Loader");
	}
}

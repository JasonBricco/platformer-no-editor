﻿using UnityEngine;
using System;

public struct Vector2i : IEquatable<Vector2i>
{
	public static readonly Vector2i Zero = new Vector2i(0, 0);
	public static readonly Vector2i Left = new Vector2i(-1, 0);
	public static readonly Vector2i Right = new Vector2i(1, 0);
	public static readonly Vector2i Up = new Vector2i(0, 1);
	public static readonly Vector2i Down = new Vector2i(0, -1);

	public static readonly Vector2i[] Directions = { Left, Right, Up, Down };

	public int x, y;

	public Vector2i(int x, int y)
	{
		this.x = x;
		this.y = y;
	}

	public Vector2i(Vector2 value)
	{
		this.x = (int)value.x;
		this.y = (int)value.y;
	}

	public bool Equals(Vector2i other)
	{
		return this.x == other.x && this.y == other.y;
	}

	public override int GetHashCode()
	{
		return x.GetHashCode() * 17 + y.GetHashCode();
	}

	public Vector2 ToVector2()
	{
		return new Vector2(x, y);
	}

	public override string ToString()
	{
		return x + ", " + y;
	}

	public static Vector2i operator + (Vector2i a, Vector2i b)
	{
		return new Vector2i(a.x + b.x, a.y + b.y);
	}

	public static Vector2i operator - (Vector2i a, Vector2i b)
	{
		return new Vector2i(a.x - b.x, a.y - b.y);
	}
}

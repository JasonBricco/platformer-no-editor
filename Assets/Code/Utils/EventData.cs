﻿using UnityEngine;

public sealed class EventData 
{
	public object arg;

	public EventData(object arg)
	{
		this.arg = arg;
	}
}

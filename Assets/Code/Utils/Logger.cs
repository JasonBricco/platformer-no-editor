﻿using UnityEngine;
using System;
using System.IO;
using System.Text;

public sealed class Logger : MonoBehaviour
{
	private static string dataPath;
	private static bool allowPrinting = false;

	private void Awake()
	{
		dataPath = Application.persistentDataPath;
		Application.logMessageReceived += HandleError;

		if (!Debug.isDebugBuild)
			enabled = false;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			allowPrinting = !allowPrinting;
			Debug.Log("Printing " + (allowPrinting ? "Enabled" : "Disabled"));
		}
	}

	private void HandleError(string logString, string stackTrace, LogType type)
	{
		if (type == LogType.Error)
		{
			Log(logString, stackTrace);
			Application.Quit();
		}
	}

	public static void Log(params string[] items)
	{
		StringBuilder text = new StringBuilder(System.DateTime.Now.ToString() + System.Environment.NewLine);

		for (int i = 0; i < items.Length; i++)
			text.AppendLine(items[i]);

		File.AppendAllText(dataPath + "/Log.txt", text.ToString() + System.Environment.NewLine);
	}

	public static void LogError(params string[] items)
	{
		Debug.LogError(items[0]);
		Log(items);
	}

	[System.Diagnostics.Conditional("DEBUG")]
	public static void Print(string message)
	{
		if (allowPrinting) Debug.Log(message);
	}
}

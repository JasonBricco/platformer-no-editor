﻿using UnityEngine;

public static class Utils
{
	public static Vector2i WorldToChunkPos(Vector3 worldPos)
	{
		return new Vector2i(Mathf.FloorToInt(worldPos.x) >> Chunk.ShiftX, Mathf.FloorToInt(worldPos.y) >> Chunk.ShiftY);
	}

	public static Vector2i LocalToTilePos(Vector2i localPos, Vector2i chunkPos)
	{
		return new Vector2i(chunkPos.x * Chunk.SizeX + localPos.x, chunkPos.y * Chunk.SizeY + localPos.y);
	}

	public static Vector2i LocalToTilePos(int localX, int localY, Vector2i chunkPos)
	{
		return new Vector2i(chunkPos.x * Chunk.SizeX + localX, chunkPos.y * Chunk.SizeY + localY);
	}

	public static Vector2i WorldToTilePos(Vector3 worldPos)
	{
		return new Vector2i(Mathf.FloorToInt(worldPos.x), Mathf.FloorToInt(worldPos.y));
	}

	public static Vector2i TileToChunkPos(Vector2i tilePos)
	{
		return new Vector2i(tilePos.x >> Chunk.ShiftX, tilePos.y >> Chunk.ShiftY);
	}

	public static Vector2i TileToChunkPos(int tileX, int tileY)
	{
		return new Vector2i(tileX >> Chunk.ShiftX, tileY >> Chunk.ShiftY);
	}

	public static Vector2i TileToLocalPos(Vector2i tilePos)
	{
		return new Vector2i(tilePos.x & Chunk.MaskX, tilePos.y & Chunk.MaskY);
	}

	public static Vector2i WorldToLocalPos(Vector3 worldPos)
	{
		int x = Mathf.FloorToInt(worldPos.x) & Chunk.MaskX;
		int y = Mathf.FloorToInt(worldPos.y) & Chunk.MaskY;

		return new Vector2i(x, y);
	}

	public static Vector2i TileToLocalPos(int tileX, int tileY)
	{
		return new Vector2i(tileX & Chunk.MaskX, tileY & Chunk.MaskY);
	}

	public static Vector3 WorldMousePos()
	{
		return Camera.main.ScreenToWorldPoint(Input.mousePosition);
	}

	public static Vector2i TileMousePos()
	{
		Vector3 worldPos = WorldMousePos();
		return new Vector2i(Mathf.FloorToInt(worldPos.x), Mathf.FloorToInt(worldPos.y));
	}

	public static Vector2i LocalMousePos()
	{
		return TileToLocalPos(TileMousePos());
	}

	public static Vector3 WorldFromChunkCorner(int cX, int cY)
	{
		return WorldFromChunkCorner(new Vector2i(cX, cY));
	}

	public static Vector3 WorldFromChunkCorner(Vector2i chunkPos)
	{
		Vector2i tilePos = LocalToTilePos(Vector2i.Zero, chunkPos);
		return new Vector3(tilePos.x, tilePos.y);
	}

	public static Vector2i TileFromChunkCorner(Vector2i chunkPos)
	{
		return LocalToTilePos(Vector2i.Zero, chunkPos);
	}

	public static Vector2 LocalToWorldPos(int localX, int localY, Vector2i chunkPos)
	{
		Vector2i tilePos = LocalToTilePos(localX, localY, chunkPos);
		return new Vector2(tilePos.x, tilePos.y);
	}

	public static Vector2i LocalToChunkPos(int localX, int localY, Vector2i cPos)
	{
		int xOffset = (int)(Mathf.Floor((float)localX / Chunk.SizeX));
		int yOffset = (int)(Mathf.Floor((float)localY / Chunk.SizeY));

		return new Vector2i(cPos.x + xOffset, cPos.y + yOffset);
	}

	public static Vector3 GetHitDirection(Collider other, Vector3 pos)
	{
		return -(other.transform.position - pos).normalized;
	}

	public static int Square(int value)
	{
		return value * value;
	}

	public static float Saturate(float value)
	{
		return Mathf.Max(0.0f, Mathf.Min(1.0f, value));
	}

	public static bool TryClamp(ref float value, float min, float max)
	{
		if (value < min) 
		{
			value = min;
			return true;
		}

		if (value > max)
		{
			value = max;
			return true;
		}

		return false;
	}

	public static float Frac(float value)
	{
		return value - Mathf.Floor(value);
	}
}

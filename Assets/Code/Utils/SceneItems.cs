﻿using UnityEngine;
using System.Collections.Generic;

public sealed class SceneItems : MonoBehaviour 
{
	private static Dictionary<string, GameObject> sceneItems = new Dictionary<string, GameObject>();

	private void Awake()
	{
		if (sceneItems.Count > 0) sceneItems.Clear();
		
		Transform[] items = GetComponentsInChildren<Transform>(true);

		for (int i = 0; i < items.Length; i++)
		{
			GameObject go = items[i].gameObject;

			try { sceneItems.Add(go.name, go); }
			catch 
			{ 
				Debug.LogError("Duplicate object found with name: " + go.name); 
				Debug.Break();
				return;
			}
		}
	}
		
	public static GameObject GetItem(string name)
	{
		GameObject item;

		if (sceneItems.TryGetValue(name, out item))
			return item;
		else
		{
			Debug.LogError("Could not find a scene item with the name: " + name);
			return null;
		}
	}

	public static T GetItem<T>(string name) where T:Component
	{
		return GetItem(name).GetComponent<T>();
	}
}

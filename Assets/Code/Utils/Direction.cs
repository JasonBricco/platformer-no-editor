﻿using UnityEngine;
using System;

public struct Direction : IEquatable<Direction>
{
	public Vector2i vector;
	public int index;

	public bool Equals(Direction other)
	{
		return this.index == other.index;
	}

	public Direction(Vector2i vector, int index)
	{
		this.vector = vector;
		this.index = index;
	}
}

public static class Directions
{
	public static readonly Direction None = new Direction(new Vector2i(0, 0), -1);
	public static readonly Direction Left = new Direction(new Vector2i(-1, 0), 0);
	public static readonly Direction Right = new Direction(new Vector2i(1, 0), 1);
	public static readonly Direction Down = new Direction(new Vector2i(0, -1), 2);
	public static readonly Direction Up = new Direction(new Vector2i(0, 1), 3);
}

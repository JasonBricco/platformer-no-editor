﻿using UnityEngine;
using System;

public struct TileInstance : IEquatable<TileInstance>
{
	public Tile tile;
	public int x, y;

	public bool Equals(TileInstance other)
	{
		return tile.ID == other.tile.ID && x == other.x && y == other.y;
	}

	public TileInstance(Tile tile, int x, int y)
	{
		this.tile = tile;
		this.x = x;
		this.y = y;
	}
}

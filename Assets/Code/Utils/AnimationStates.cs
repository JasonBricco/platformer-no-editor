﻿using UnityEngine;

public static class AnimationStates
{
	private static int idle = Animator.StringToHash("Idle");
	private static int walk = Animator.StringToHash("Walk");
	private static int spin = Animator.StringToHash("Spin");

	public static int Idle { get { return idle; } }
	public static int Walk { get { return walk; } }
	public static int Spin { get { return spin; } }
}

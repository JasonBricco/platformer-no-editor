﻿using UnityEngine;

public static class SortingLayers
{
	private static int main = SortingLayer.NameToID("Default");
	private static int fluid = SortingLayer.NameToID("Fluid");
	private static int decoration = SortingLayer.NameToID("Decoration");
	private static int wall = SortingLayer.NameToID("Wall");

	public static int Main { get { return main; } }
	public static int Fluid { get { return fluid; } }
	public static int Decoration { get { return decoration; } }
	public static int Wall { get { return wall; } }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public sealed class CommandProcessor
{
	private Char[] delimiters = new char[] { ' ' };

	public void Process(string input)
	{
		if (input.Length == 0) return;

		string lower = input.ToLower();
		string[] parts = lower.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

		Function function = null;

		bool success = Function.Library.TryGetFunction(parts[0], out function);

		if (!success)
		{
			EventManager.Notify("ReportCommand", new EventData("Invalid command entered."));
			return;
		}
			
		function.Compute(parts);
	}
}

﻿using UnityEngine;
using System;

public class Function
{
	protected static FunctionLibrary library = new FunctionLibrary();
	public static FunctionLibrary Library { get { return library; } }

	public virtual void Compute(string[] args) {}

	protected bool GetInteger(string arg, out int num)
	{
		if (int.TryParse(arg, out num))
			return true;

		return false;
	}

	protected bool GetBool(string arg, out bool b)
	{
		b = false; 

		if (arg == "true")
		{
			b = true;
			return true;
		}
		else if (arg == "false")
		{
			b = false;
			return true;
		}
	
		return false;
	}

	protected bool CheckArgCount(string[] args, int required, string usage)
	{
		if (args.Length < required)
		{
			EventManager.Notify("ReportCommand", new EventData(usage));
			return false;
		}

		return true;
	}
}

﻿using UnityEngine;

public sealed class LoadFunc : Function
{
	public override void Compute(string[] args)
	{
		if (!CheckArgCount(args, 2, "Usage: load index")) return;

		int level;

		if (!GetInteger(args[1], out level)) 
		{
			EventManager.Notify("ReportCommand", new EventData("Argument must be an integer (the index of the level to load)."));
			return;
		}

		EventManager.Notify("ReportCommand", null);
		EventManager.Notify("LoadLevel", new EventData(level));
	}
}

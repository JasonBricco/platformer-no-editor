﻿using UnityEngine;
using System.Collections.Generic;

public sealed class FunctionLibrary 
{
	private Dictionary<string, Function> functions = new Dictionary<string, Function>();

	public FunctionLibrary()
	{
		functions.Add("load", new LoadFunc());
		functions.Add("loadnext", new LoadNextLevelFunc());
		functions.Add("shortlevels", new ShortLevelsFunc());
	}

	public Function GetFunction(string name)
	{
		return functions[name];
	}

	public bool TryGetFunction(string name, out Function function)
	{
		return functions.TryGetValue(name, out function);
	}
}

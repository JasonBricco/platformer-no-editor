﻿using UnityEngine;

public sealed class LoadNextLevelFunc : Function
{
	public override void Compute(string[] args)
	{
		EventManager.Notify("ReportCommand",  null);
		EventManager.Notify("BuildNextLevel", null);
	}
}

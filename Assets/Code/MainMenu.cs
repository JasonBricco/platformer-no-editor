﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class MainMenu : MonoBehaviour 
{
	public void Play()
	{
		SceneManager.LoadScene(Scenes.Game);
	}

	public void Quit()
	{
		Application.Quit();
	}
}

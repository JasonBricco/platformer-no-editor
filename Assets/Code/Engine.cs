﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections.Generic;
using System.IO;

public static class CollisionLayer
{
	public const int Enemy = 8, Player = 9, Terrain = 10, Entity = 11, CollidableEntity = 12, Projectile = 13;
}

public static class Scenes
{
	public const int Loader = 0, Game = 1, MainMenu = 2, Lose = 3;
}

public static class StoreLayer
{
	public const int Main = 0, Fluid = 1, Decoration = 2, Wall = 3;
}

public enum DeviceType
{
	PC, iPhone, iPad
}

public delegate void Method();

public sealed class Engine : MonoBehaviour 
{
	public static bool IsPaused { get; private set; }
	public static DeviceType Device { get; private set; }

	private void Awake()
	{
		Application.targetFrameRate = 60;

		switch (Application.platform)
		{
		case RuntimePlatform.OSXPlayer:
		case RuntimePlatform.WindowsPlayer:
			Device = DeviceType.PC;
			break;

		case RuntimePlatform.IPhonePlayer:
			if (SystemInfo.deviceModel.Contains("iPad"))
				Device = DeviceType.iPad;
			else Device = DeviceType.iPhone;
			break;
		}
	}

	public static void Freeze()
	{
		IsPaused = true;
		Time.timeScale = 0.0f;
	}

	public static void Unfreeze()
	{
		Time.timeScale = 1.0f;
		IsPaused = false;
	}

	public static void Pause(bool pauseMenu = true)
	{
		if (!IsPaused)
		{
			Freeze();
			GC.Collect();
			SceneItems.GetItem<ScreenFader>("Camera").SetPause();

			if (pauseMenu)
				SceneItems.GetItem("PauseMenu").SetActive(true);
		}
	}

	public static void Unpause()
	{
		if (IsPaused)
		{
			SceneItems.GetItem("PauseMenu").SetActive(false);
			SceneItems.GetItem<ScreenFader>("Camera").RemovePause();
			Unfreeze();
		}
	}

	public void Continue()
	{
		Unpause();
	}

	public void GoToMenu()
	{
		SceneManager.LoadScene(Scenes.MainMenu);
	}

	private void OnDestroy()
	{
		Unpause();
	}
}

﻿using UnityEngine;

public sealed class TreeTile : Tile 
{
	public TreeTile(int data)
	{
		ID = TileID.Tree;
		Data = (byte)data;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new TreeTile(Data);
	}

	public override int RenderLayer()
	{
		return SortingLayers.Decoration; 
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos)
	{
		base.Activate(map, tPos, false);
		renderer.transform.position = new Vector3(tPos.x + 0.5f, tPos.y);
	}
}

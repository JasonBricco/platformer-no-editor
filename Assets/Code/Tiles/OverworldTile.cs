﻿using UnityEngine;

public sealed class OverworldTile : Tile 
{
	public OverworldTile(int data)
	{
		ID = TileID.Overworld;
		Data = (byte)data;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new OverworldTile(Data);
	}
}

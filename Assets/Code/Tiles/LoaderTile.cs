﻿using UnityEngine;

public sealed class LoaderTile : Tile 
{
	public LoaderTile(int data)
	{
		ID = TileID.Loader;
	}

	public override Tile Clone() 
	{
		return new LoaderTile(Data);
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos) {}
	public override void Deactivate(Map map, Vector2i tPos) {}

	public override bool IsPassable()
	{
		return true;
	}

	public override void OnEnter(Map map, Entity entity, int x, int y)
	{
		if (entity is Player)
			map.BuildNextLevel();
	}
}

﻿using UnityEngine;

public sealed class WaterTile : Tile 
{
	public WaterTile(int data)
	{
		ID = TileID.Water;
		Data = (byte)data;
		MaterialID = 2;
	}

	public override Tile Clone()
	{
		return new WaterTile(Data);
	}

	public override int RenderLayer()
	{
		return SortingLayers.Fluid; 
	}

	public override bool IsPassable()
	{
		return true;
	}
}

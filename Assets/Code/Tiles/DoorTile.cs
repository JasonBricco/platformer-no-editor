﻿using UnityEngine;

public sealed class DoorTile : Tile 
{
	public DoorTile(int data)
	{
		ID = TileID.Door;
		Data = (byte)data;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new DoorTile(Data);
	}

	public override int RenderLayer()
	{
		return SortingLayers.Decoration; 
	}

	public override void Update(Map map, int x, int y)
	{
		Vector2i playerPos = map.Player.TilePos;

		if (playerPos.x == x && playerPos.y == y)
		{
			if (GameInput.GetY() > 0.5f)
			{
				renderer.sprite = map.GetTexture(ID, 1);
				map.BuildNextLevel();
			}
		}
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos)
	{
		base.Activate(map, tPos, false);
		renderer.transform.position = new Vector3(tPos.x + 0.5f, tPos.y);
		map.StartTileUpdates(this, tPos.x, tPos.y);
	}

	public override void Deactivate(Map map, Vector2i tPos)
	{
		map.StopTileUpdates(this, tPos.x, tPos.y);
		base.Deactivate(map, tPos);
	}
}

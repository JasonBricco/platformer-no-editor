﻿using UnityEngine;

public sealed class CastleTile : Tile 
{
	public CastleTile(int data)
	{
		ID = TileID.Castle;
		Data = (byte)data;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new CastleTile(Data);
	}
}

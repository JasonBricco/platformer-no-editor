﻿using UnityEngine;

public sealed class TorchTile : Tile 
{
	public TorchTile()
	{
		ID = TileID.Torch;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new TorchTile();
	}

	public override bool IsPassable()
	{
		return true;
	}
 
	public override void OnSet(Chunk chunk, int x, int y)
	{
		GameObject light = LightFactory.GetLight(12);

		Vector2 worldPos = Utils.LocalToWorldPos(x, y, chunk.Position);
		worldPos.x += 0.5f;
		worldPos.y += 0.5f;

		light.GetComponent<Transform>().position = worldPos;
	}
}

﻿using UnityEngine;

public sealed class ChestTile : Tile 
{
	public ChestTile()
	{
		ID = TileID.Chest;
		MaterialID = 0;
	}

	public override int RenderLayer()
	{
		return SortingLayers.Decoration;
	}

	public override Tile Clone()
	{
		return new ChestTile();
	}

	public override bool IsPassable()
	{
		return true;
	}

	public override void Update(Map map, int x, int y)
	{
		Vector2i playerPos = map.Player.TilePos;

		if (playerPos.x == x && playerPos.y == y)
		{
			if (GameInput.GetY() > 0.5f)
			{
				map.Player.EnableFireball();
				map.SetTile(x, y, Tile.Air, 2);
			}
		}
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos)
	{
		base.Activate(map, tPos, setPos);
		map.StartTileUpdates(this, tPos.x, tPos.y);
	}

	public override void Deactivate(Map map, Vector2i tPos)
	{
		map.StopTileUpdates(this, tPos.x, tPos.y);
		base.Deactivate(map, tPos);
	}
}

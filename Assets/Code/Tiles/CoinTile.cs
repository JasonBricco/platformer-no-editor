﻿using UnityEngine;

public sealed class CoinTile : Tile 
{
	public CoinTile()
	{
		ID = TileID.Coin;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new CoinTile();
	}

	public override bool IsPassable()
	{
		return true;
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos)
	{
		base.Activate(map, tPos, setPos);
		Animator animator = renderer.GetComponent<Animator>();
		animator.enabled = true;
		animator.runtimeAnimatorController = map.GetAnimationController(0);
		animator.Play(AnimationStates.Spin);
	}

	public override void Deactivate(Map map, Vector2i tPos)
	{
		Animator animator = renderer.GetComponent<Animator>();
		animator.Stop();
		animator.enabled = false;
		base.Deactivate(map, tPos);
	}

	public override void OnEnter(Map map, Entity entity, int x, int y)
	{
		if (entity is Player)
		{
			map.AddCoins(1);
			map.SetTile(x, y, Tile.Air, 0);
		}
	}
}

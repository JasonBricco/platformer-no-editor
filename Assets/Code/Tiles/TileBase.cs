﻿using UnityEngine;
using System;

public static class TileID
{
	public const ushort Air = 0, Overworld = 1, Torch = 2, Wall = 3, Coin = 4, Water = 5, Castle = 6, 
	Chest = 7, Tree = 8, Chandelier = 9, Door = 10, Loader = 11;
}

public class TileBase : IEquatable<Tile>
{
	public static readonly Tile Air = new AirTile();

	public ushort ID { get; protected set; }
	public byte Data { get; protected set; }
	public byte MaterialID { get; protected set; }

	protected SpriteRenderer renderer;

	public bool Equals(Tile other)
	{
		return ID == other.ID && Data == other.Data;
	}

	public bool Equals(ushort ID)
	{
		return this.ID == ID;
	}

	public virtual Tile Clone() 
	{
		throw new NotImplementedException();
	}

	public virtual void Update(Map map, int x, int y) {}

	public virtual int RenderLayer()
	{
		return 0;
	}

	public bool IsActive()
	{
		return renderer != null;
	}

	public virtual void Activate(Map map, Vector2i tPos, bool setPos)
	{
		renderer = map.GetSprite();
		renderer.enabled = true;
		renderer.sprite = map.GetTexture(ID, Data);
		renderer.material = map.GetMaterial(MaterialID);
		renderer.sortingLayerID = RenderLayer();

		if (setPos) renderer.transform.position = new Vector3(tPos.x, tPos.y);
	}

	public virtual void Deactivate(Map map, Vector2i tPos)
	{
		map.ReturnSprite(renderer);
		renderer = null;
	}
		
	public virtual bool IsPassable()
	{
		return false;
	}

	public virtual void OnSet(Chunk chunk, int x, int y) {}

	public virtual void OnDelete(Chunk chunk, int x, int y) {}

	public virtual void OnEnter(Map map, Entity entity, int x, int y) {}
}

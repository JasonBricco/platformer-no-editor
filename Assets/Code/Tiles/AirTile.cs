﻿using UnityEngine;

public sealed class AirTile : Tile 
{
	public AirTile()
	{
		ID = TileID.Air;
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos) {}
	public override void Deactivate(Map map, Vector2i tPos) {}

	public override Tile Clone()
	{
		return Tile.Air;
	}

	public override bool IsPassable()
	{
		return true;
	}
}

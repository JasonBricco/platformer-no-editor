﻿using UnityEngine;

public sealed class WallTile : Tile 
{
	public WallTile(int data)
	{
		ID = TileID.Wall;
		Data = (byte)data;
		MaterialID = 1;
	}

	public override Tile Clone()
	{
		return new WallTile(Data);
	}

	public override int RenderLayer()
	{
		return SortingLayers.Wall; 
	}

	public override bool IsPassable()
	{
		return true;
	}
}

﻿using UnityEngine;

public sealed class ChandelierTile : Tile 
{
	public ChandelierTile()
	{
		ID = TileID.Chandelier;
		MaterialID = 0;
	}

	public override Tile Clone()
	{
		return new ChandelierTile();
	}

	public override int RenderLayer()
	{
		return SortingLayers.Decoration; 
	}

	public override void Activate(Map map, Vector2i tPos, bool setPos)
	{
		base.Activate(map, tPos, false);
		renderer.transform.position = new Vector3(tPos.x + 0.5f, tPos.y + 1.0f);
	}

	public override void OnSet(Chunk chunk, int x, int y)
	{
		GameObject light = LightFactory.GetLight(24);

		float offset = (144.0f / GameCamera.PixelsPerUnit) * 0.9f;

		Vector2 wPos = Utils.LocalToWorldPos(x, y, chunk.Position);
		Vector3 setPos = new Vector3(wPos.x + 0.5f, wPos.y - offset);

		light.GetComponent<Transform>().position = setPos;
	}
}

﻿using UnityEngine;
using System.Collections.Generic;

public sealed class VoidGenerator : Generator
{
	public override void Generate(Map map, Generator next, int seed)
	{
		map.SetSize(1, 1);
		map.SetBackdrop(null);
		map.SetChunkActive(Vector2i.Zero);
		map.SpawnPlayer(Chunk.SizeX / 2, Chunk.SizeY / 2);
	}
}

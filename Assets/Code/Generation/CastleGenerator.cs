﻿using UnityEngine;

public sealed class CastleGenerator : Generator
{
	public CastleGenerator()
	{
		Indoor = true;
		Name = "Castle";
	}

	public override void Generate(Map map, Generator next, int seed)
	{
		this.map = map;

		int mapWidth = map.ShortLevels ? 1 : Random.Range(8, 16);
		int mapHeight = 1;

		map.SetSize(mapWidth, mapHeight);

		for (int cX = 0; cX < mapWidth; cX++)
		{
			Chunk chunk = map.GetChunk(cX, 0, true);
			chunk.SetRect(0, 0, Chunk.SizeX - 1, 0, new CastleTile(1), StoreLayer.Main);
			chunk.Fill(new WallTile(0), StoreLayer.Wall);
		}

		int chandelierHeight = 7;

		for (int tX = 12; tX < map.TileLength; tX += 12)
		{
			for (int offset = -1; offset <= 1; offset++)
				map.SetTile(tX + offset, chandelierHeight + 1, new CastleTile(2), StoreLayer.Main);

			map.SetTile(tX, chandelierHeight, new ChandelierTile(), StoreLayer.Decoration);
		}

		int spawnX = 5, spawnY = 2;

		for (int gapIndex = spawnX + 5; gapIndex < map.TileLength; gapIndex += Random.Range(8, 15))
		{
			int gapLength = Random.Range(2, 6);

			for (int j = 0; j < gapLength; j++)
				map.SetTile(gapIndex + j, 0, Tile.Air, StoreLayer.Main);
		}

		map.SpawnPlayer(spawnX, spawnY);

		for (int zombieCount = Random.Range(2, 6); zombieCount > 0; zombieCount--)
		{
			int randX = Random.Range(spawnX + 3, map.TileLength - 2);
			Vector2i surface = map.NearestSurface(randX, 2, 0, 2);
			map.CreateEntity("Zombie", surface.x, surface.y + 1);
		}

		if (!PlaceExitLoaders(next)) 
			MakeDoor(map.TileLength - 6, 1);

		map.SetBackdrop(null);
		map.SetDark();
		map.SetChunkActive(Vector2i.Zero);
	}
}

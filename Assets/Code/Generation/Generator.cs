﻿using UnityEngine;

public class Generator 
{
	protected Map map;

	public bool Indoor { get; protected set; }
	public string Name { get; protected set; }

	public virtual void Generate(Map map, Generator next, int seed) {}

	protected bool PlaceExitLoaders(Generator next)
	{
		int x = map.TileLength - 1;
		int surface = map.GetSurface(x, 2, 0, map.TileHeight);

		if (Indoor == next.Indoor)
		{
			for (int y = surface + 1; y < map.TileHeight; y++)
				map.SetTile(x, y, new LoaderTile(0), StoreLayer.Main);

			return true;
		}
			
		return false;
	}

	protected void MakeDoor(int startX, int startY, Tile floor = null)
	{
		for (int y = startY; y < startY + 4; y++)
		{
			for (int x = startX - 1; x <= startX + 1; x++)
				map.SetTile(x, y, new WallTile(0), StoreLayer.Wall);
		}

		for (int x = startX - 1; x <= startX + 1; x++)
			map.SetTile(x, startY - 1, floor.Clone(), StoreLayer.Main);

		map.SetTile(startX, startY, new DoorTile(0), StoreLayer.Decoration);
	}

	protected float Noise(float x, float scale, float persistence, int octaves, int seed) 
	{
		x = (x * scale) + seed;

		float total = 0;
		float frq = 1, amp = 1;

		for (int i = 0; i < octaves; i++) 
		{
			if (i >= 1) 
			{
				frq *= 2;
				amp *= persistence;
			}

			total += Mathf.PerlinNoise(x * frq, 0) * amp;
		}

		return total;
	}
}

﻿using UnityEngine;

public sealed class SecretCastleGenerator : Generator
{
	public SecretCastleGenerator()
	{
		Indoor = true;
		Name = "Secret Castle";
	}

	public override void Generate(Map map, Generator next, int seed)
	{
		this.map = map;

		map.SetSize(1, 1);
		Chunk chunk = map.GetChunk(0, 0, true);

		chunk.SetRect(0, 0, Chunk.SizeX - 1, 0, new CastleTile(0), StoreLayer.Main);
		chunk.SetRect(13, 1, 17, 1, new CastleTile(0), StoreLayer.Main);
		chunk.SetTile(15, 2, new ChestTile(), StoreLayer.Decoration);
		chunk.Fill(new WallTile(0), StoreLayer.Wall);

		chunk.SetRect(13, 9, 17, 9, new CastleTile(1), StoreLayer.Main);
		chunk.SetTile(15, 8, new ChandelierTile(), StoreLayer.Decoration);

		map.SpawnPlayer(2, 2);

		for (int i1 = 9; i1 <= 13; i1 += 2)
			map.CreateEntity("Zombie", i1, 2.0f);

		for (int i2 = 17; i2 <= 21; i2 += 2)
			map.CreateEntity("Zombie", i2, 2.0f);

		if (!PlaceExitLoaders(next)) MakeDoor(map.TileLength - 2, 1);

		map.SetBackdrop(null);
		map.SetDark();
		map.SetChunkActive(Vector2i.Zero);
	}
}

﻿using UnityEngine;
using System.Collections.Generic;

public sealed class TestGenerator : Generator
{
	public TestGenerator()
	{
		Indoor = false;
		Name = "Test";
	}

	public override void Generate(Map map, Generator next, int seed)
	{
		this.map = map;

		map.SetSize(1, 1);
		Chunk chunk = map.GetChunk(0, 0, true);
		chunk.SetRect(0, 0, Chunk.SizeX - 1, 0, new OverworldTile(0), StoreLayer.Main);

		if (!PlaceExitLoaders(next)) MakeDoor(Chunk.SizeX - 2, 1);

		map.SetLight();
		map.SetBackdrop("Overworld");
		map.SetChunkActive(Vector2i.Zero);
		map.SpawnPlayer(2, 2);
	}
}

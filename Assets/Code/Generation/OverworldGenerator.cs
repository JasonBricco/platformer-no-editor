﻿using UnityEngine;
using System.Collections.Generic;

public sealed class OverworldGenerator : Generator
{
	public OverworldGenerator()
	{
		Indoor = false;
		Name = "Overworld";
	}

	public override void Generate(Map map, Generator next, int seed)
	{
		this.map = map;

		float scale = 5.0f / 100.0f;
		float persistence = 0.5f;
		int octaves = 1;

		int mapWidth = map.ShortLevels ? 1 : Random.Range(8, 16);
		int mapHeight = 1;

		map.SetSize(mapWidth, mapHeight);

		int treeFrequency = 4;
		Queue<Vector2i> trees = new Queue<Vector2i>(map.TileLength / treeFrequency);

		int coinHeight = 0, coinSpace = 0;

		for (int x = 0; x < map.TileLength; x++)
		{
			int height = (int)(Noise(x, scale, persistence, octaves, seed) * 10.0f);

			if (height != coinHeight || x == map.TileLength - 1)
			{
				if (coinSpace >= 9)
				{
					int mid = x - (coinSpace / 2) - 1;
					int count = Mathf.Min((coinSpace / 4), 5);

					for (int i = -count; i <= count; i++)
						map.SetTile(mid + i, coinHeight, new CoinTile(), StoreLayer.Main);
				}

				coinHeight = height;
				coinSpace = 0;
			}
			else coinSpace++;

			for (int y = 0; y < height; y++)
			{
				Tile tile = y == height - 1 ? new OverworldTile(1) : new OverworldTile(2);
				map.SetTile(x, y, tile, StoreLayer.Main);
			}
				
			if ((x % treeFrequency) == 0)
			{
				if (Random.Range(0, 2) == 0)
					trees.Enqueue(new Vector2i(x, height));
			}
		}

		while (trees.Count > 0)
		{
			Vector2i pos = trees.Dequeue();

			if (map.PassableRegion(pos.x - 1, pos.y, pos.x + 1, pos.y))
				map.SetTile(pos.x, pos.y, new TreeTile(Random.Range(0, 2)), StoreLayer.Decoration);
		}

		int spawnX = 5, spawnY = 4;
		map.SpawnPlayer(spawnX, spawnY);

		for (int zombieCount = Random.Range(2, 6); zombieCount > 0; zombieCount--)
		{
			int randX = Random.Range(spawnX + 3, map.TileLength - 2);
			map.CreateEntity("Zombie", randX, map.GetSurface(randX, 2, 0, map.TileHeight) + 1);
		}

		if (!PlaceExitLoaders(next))
		{
			int surface = map.GetSurface(map.TileLength - 6, 2, 0, map.TileHeight);
			MakeDoor(map.TileLength - 6, surface + 1);
		}
			
		map.SetBackdrop("Overworld");
		map.SetLight();
		map.SetChunkActive(Vector2i.Zero);
	}
}

﻿using UnityEngine;

public enum InputMode
{
	Keyboard,
	Touch,
	Controller
}

public sealed class GameInput : MonoBehaviour 
{
	private static InputMode mode;
	private static VirtualJoystick joystick;
	private static bool doJump;
	private static bool doFire;

	private void Start()
	{
		if (Engine.Device == DeviceType.iPad || Engine.Device == DeviceType.iPhone)
			mode = InputMode.Touch;
		else mode = InputMode.Keyboard;

		joystick = SceneItems.GetItem<VirtualJoystick>("JoystickBase");
	}

	public void SetJump()
	{
		doJump = true;
	}

	public void SetFire()
	{
		doFire = true;
	}

	public void Pause()
	{
		Engine.Pause();
	}

	private void Update()
	{
		switch (mode)
		{
		case InputMode.Keyboard:
			if (Input.GetKeyDown(KeyCode.Escape))
				Engine.Pause();
			break;
		}
	}

	public static float GetMove()
	{
		switch (mode)
		{
		case InputMode.Keyboard:
			return Input.GetAxisRaw("X");

		case InputMode.Touch:
			if (joystick.InputDir.x < -0.1f) return -1.0f;
			if (joystick.InputDir.x > 0.1f) return 1.0f;
			return 0.0f;
		}

		return 0.0f;
	}

	public static float GetY()
	{
		switch (mode)
		{
		case InputMode.Keyboard:
			return Input.GetAxisRaw("Y");

		case InputMode.Touch:
			if (joystick.InputDir.y < -0.1f) return -1.0f;
			if (joystick.InputDir.y > 0.1f) return 1.0f;
			return 0.0f;
		}

		return 0.0f;
	}

	public static bool DoJump()
	{
		switch (mode)
		{
		case InputMode.Keyboard:
			return Input.GetKey(KeyCode.Space);

		case InputMode.Touch:
			bool shouldJump = doJump;
			doJump = false;
			return shouldJump;
		}

		return false;
	}

	public static bool DoFire()
	{
		switch (mode)
		{
		case InputMode.Keyboard:
			return Input.GetMouseButtonDown(0);

		case InputMode.Touch:
			bool shouldFire = doFire;
			doFire = false;
			return shouldFire;
		}

		return false;
	}
}

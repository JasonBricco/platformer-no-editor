﻿using UnityEngine;

public sealed class VirtualControls : MonoBehaviour 
{
	private void Awake()
	{
		if (Application.platform != RuntimePlatform.IPhonePlayer)
			gameObject.SetActive(false);
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public sealed class HealthDisplay : MonoBehaviour
{
	[SerializeField] private Sprite[] sprites;
	private Image[] hearts;

	private void Awake()
	{
		hearts = GetComponentsInChildren<Image>();
	}

	public void Recompute(int health)
	{
		for (int i = 0; i < hearts.Length; i++)
		{
			health = Mathf.Max(health, 0);

			if (health >= 4)
			{
				hearts[i].sprite = sprites[4];
				health -= 4;
				continue;
			}

			hearts[i].sprite = sprites[health];
			health = 0;
		}
	}
}

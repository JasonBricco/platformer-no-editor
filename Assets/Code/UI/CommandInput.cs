﻿using UnityEngine;
using UnityEngine.UI;

public class CommandInput : MonoBehaviour 
{
	private GameObject commandUI;
	private InputField field;
	private Text commandError;

	private CommandProcessor processor = new CommandProcessor();

	private void Start()
	{
		commandUI = SceneItems.GetItem("CommandUI");
		field = SceneItems.GetItem<InputField>("CommandField");
		commandError = SceneItems.GetItem<Text>("CommandError");

		EventManager.StartListening("ReportCommand", HandleCommandReport);
	}

	private void Update()
	{
		if (commandUI.activeSelf)
		{
			if (Input.GetKey(KeyCode.Return))
				SubmitCommand(field);
		}

		if (Engine.IsPaused) return;

		if (Input.GetKeyDown(KeyCode.Slash))
		{
			Engine.Pause(false);
			commandUI.SetActive(true);
			field.ActivateInputField();
		}
	}

	public void SubmitCommand(InputField input)
	{
		commandError.enabled = false;
		processor.Process(input.text);
	}

	public void Disable()
	{
		commandUI.SetActive(false);
		commandError.enabled = false;
		field.text = "";
		Engine.Unpause();
	}

	private void HandleCommandReport(EventData data)
	{
		if (data != null)
		{
			commandError.text = (string)data.arg;
			commandError.enabled = true;
		}
		else Disable();
	}
}

﻿using UnityEngine;
using UnityEngine.UI;

public sealed class UIScaler : MonoBehaviour 
{
	private void Start()
	{
		CanvasScaler scaler = GetComponent<CanvasScaler>();
		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
		scaler.matchWidthOrHeight = 1.0f;

		switch (Engine.Device)
		{
		case DeviceType.PC:
			scaler.referenceResolution = new Vector2(1024.0f, 512.0f);
			break;

		case DeviceType.iPhone:
			scaler.referenceResolution = new Vector2(1024.0f, 576.0f);
			break;

		case DeviceType.iPad:
			scaler.referenceResolution = new Vector2(1024.0f, 768.0f);
			break;
		}
	}
}

﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public sealed class LoseUI : MonoBehaviour 
{
	[SerializeField] private Text score;
	[SerializeField] private Text coins;

	private void Awake()
	{
		DataTransfer transfer = GameObject.FindWithTag("Carrier").GetComponent<DataTransfer>();
		score.text = "Score: " + transfer.score;
		coins.text = "Coins: " + transfer.coins;
	}

	public void TryAgain()
	{
		SceneManager.LoadScene(Scenes.Game);
	}

	public void Menu()
	{
		SceneManager.LoadScene(Scenes.MainMenu);
	}
}

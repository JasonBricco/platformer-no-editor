﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public sealed class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler 
{
	private RectTransform container;
	private RectTransform joystick;

	public Vector3 InputDir { get; private set; }

	private void Start()
	{
		container = GetComponent<RectTransform>();
		joystick = transform.GetChild(0).GetComponent<RectTransform>();
		InputDir = Vector3.zero;
	}

	public void OnDrag(PointerEventData data)
	{
		Vector2 position = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(container, data.position, data.pressEventCamera, out position);

		position.x = (position.x / container.sizeDelta.x);
		position.y = (position.y / container.sizeDelta.y);

		float x = (container.pivot.x == 1.0f) ? position.x * 2.0f + 1.0f : position.x * 2.0f - 1.0f;
		float y = (container.pivot.y == 1.0f) ? position.y * 2.0f + 1.0f : position.y * 2.0f - 1.0f;

		InputDir = new Vector3(x, y);
		InputDir = (InputDir.magnitude > 1.0f) ? InputDir.normalized : InputDir;

		float xPos = InputDir.x * (container.sizeDelta.x / 3.0f);
		float yPos = InputDir.y * (container.sizeDelta.y / 3.0f);

		joystick.anchoredPosition = new Vector3(xPos, yPos);
	}

	public void OnPointerDown(PointerEventData data)
	{
		OnDrag(data);
	}

	public void OnPointerUp(PointerEventData data)
	{
		InputDir = Vector3.zero;
		joystick.anchoredPosition = Vector3.zero;
	}
}

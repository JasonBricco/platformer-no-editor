﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

[System.Serializable]
public sealed class SpriteArray
{
	public Sprite[] sprites;

	public Sprite this[int i]
	{
		get { return sprites[i]; }
	}
}

public sealed class Map : MonoBehaviour
{
	[SerializeField] private GameObject spritePrefab;
	[SerializeField] private SpriteArray[] textures;
	[SerializeField] private Material[] materials;
	[SerializeField] private RuntimeAnimatorController[] controllers;

	private Chunk[,] chunks;
	private List<Chunk> loadedChunks = new List<Chunk>(16);

	public int TileLength { get; private set; }
	public int TileHeight { get; private set; }

	private GameCamera cam;

	private List<Entity> simulate = new List<Entity>(128);
	private List<Entity> notifyChunkChange = new List<Entity>(128);

	private int nextEntityID = 0;
	public int NextEntityID { get { return nextEntityID++; } }

	private int score = 0;
	private int coins = 0;

	private Text scoreUI;
	private Text coinsUI;

	private Queue<SpriteRenderer> spritePool = new Queue<SpriteRenderer>(4096);

	private List<TileInstance> tileUpdates = new List<TileInstance>();

	public Player Player { get; private set; }

	private int levelCount = 0;

	private Dictionary<int, Generator> generators = new Dictionary<int, Generator>()
	{
		{ -1, new TestGenerator() },
		{ 0, new OverworldGenerator() },
		{ 1, new CastleGenerator() },
		{ 2, new SecretCastleGenerator() }
	};

	private Generator currentLevel;
	private Generator nextLevel;

	public bool ShortLevels { get; private set; }

	private bool buildingLevel;
	 
	private void Awake()
	{
		EventManager.Clear();
	}

	private void Start()
	{
		EntityFactory.Load();
		BackdropFactory.Load();
		LightFactory.Load();

		cam = SceneItems.GetItem<GameCamera>("Camera");
		scoreUI = SceneItems.GetItem<Text>("Score");
		coinsUI = SceneItems.GetItem<Text>("Coins");

		Player = SceneItems.GetItem<Player>("Player");
		Player.Initialize(this, 0.0f, 0.0f, NextEntityID);

		EventManager.StartListening("BuildNextLevel", (data) => BuildNextLevel());
		EventManager.StartListening("LoadLevel", (data) => ForceLoad((int)data.arg));
		EventManager.StartListening("ShortLevels", (data) => ShortLevels = !ShortLevels);

		nextLevel = new OverworldGenerator();
		Generate();
	}

	public Vector2i GetSize()
	{
		return new Vector2i(chunks.GetLength(0), chunks.GetLength(1));
	}

	public void SetSize(int chunkWidth, int chunkHeight)
	{
		chunks = new Chunk[chunkWidth, chunkHeight];
		cam.SetBoundaries(chunkWidth, chunkHeight);

		TileLength = chunkWidth * Chunk.SizeX;
		TileHeight = chunkHeight * Chunk.SizeY;
	}

	public void BeginSimulating(Entity entity)
	{
		simulate.Add(entity);
	}

	public void EndSimulating(Entity entity)
	{
		simulate.Remove(entity);
	}

	public void NotifyForChunkChange(Entity entity)
	{
		notifyChunkChange.Add(entity);
	}

	public void EndNotifyForChunkChange(Entity entity)
	{
		notifyChunkChange.Remove(entity);
	}

	public Sprite GetTexture(int i, int j)
	{
		return textures[i - 1][j];
	}

	public Material GetMaterial(int meshIndex)
	{
		return materials[meshIndex];
	}

	public RuntimeAnimatorController GetAnimationController(int i)
	{
		return controllers[i];
	}

	public SpriteRenderer GetSprite()
	{
		if (spritePool.Count > 0)
			return spritePool.Dequeue();

		GameObject spriteObj = Instantiate<GameObject>(spritePrefab);
		return spriteObj.GetComponent<SpriteRenderer>();
	}

	public void ReturnSprite(SpriteRenderer rend)
	{
		rend.enabled = false;
		spritePool.Enqueue(rend);
	}
		
	public Chunk GetChunk(Vector2i cPos, bool create = false)
	{
		return GetChunk(cPos.x, cPos.y, create);
	}

	public Chunk GetChunk(int cX, int cY, bool create = false)
	{
		if (!InBounds(cX, cY))
			return null;

		Chunk chunk = chunks[cX, cY];

		if (create && chunk == null)
		{
			chunk = new Chunk(cX, cY, this);
			chunks[cX, cY] = chunk;
		}

		return chunk;
	}

	public Tile GetTile(int tileX, int tileY, int layer)
	{
		Chunk chunk = GetChunk(Utils.TileToChunkPos(tileX, tileY));
		return chunk != null ? chunk.GetTile(Utils.TileToLocalPos(tileX, tileY), layer) : Tile.Air;
	}

	public Tile GetTile(Vector2i tilePos, int layer)
	{
		return GetTile(tilePos.x, tilePos.y, layer);
	}

	public void SetTile(int tileX, int tileY, Tile tile, int layer)
	{
		Chunk chunk = GetChunk(Utils.TileToChunkPos(tileX, tileY), true);

		if (chunk != null)
		{
			Vector2i pos = Utils.TileToLocalPos(tileX, tileY);
			chunk.SetTile(pos.x, pos.y, tile, layer);
		}
	}

	public bool PassableRegion(int startX, int startY, int endX, int endY)
	{
		for (int y = startY; y <= endY; y++)
		{
			for (int x = startX; x <= endX; x++)
			{
				if (!GetTile(x, y, 0).IsPassable())
					return false;
			}
		}

		return true;
	}

	public Vector2i NearestSurface(int x, int requiredSpace, int minY, int maxY)
	{
		int surface = GetSurface(x, requiredSpace, minY, maxY);

		while (surface == -1)
		{
			x = (x + 1) % TileLength;
			surface = GetSurface(x, requiredSpace, minY, maxY);
		}
			
		return new Vector2i(x, surface);
	}

	public int GetSurface(int x, int requiredSpace, int minY, int maxY)
	{
		int firstSolid = -1;

		for (int b = minY; b <= maxY; b++)
		{
			if (!GetTile(x, b, 0).IsPassable())
			{
				firstSolid = b;
				break;
			}
		}

		if (firstSolid == -1) return -1;

		for (int y = firstSolid; y <= maxY; y++)
		{
			if (PassableRegion(x, y + 1, x, y + requiredSpace))
				return y;
		}

		return -1;
	}

	public void AddScore(int value)
	{
		score += value;
		scoreUI.text = "Score: " + score.ToString();
	}

	public void AddCoins(int value)
	{
		coins += value;
		coinsUI.text = "Coins: " + coins.ToString();
	}

	public bool IsChunkActive(Vector2i pos)
	{
		for (int i = 0; i < loadedChunks.Count; i++)
		{
			if (pos.Equals(loadedChunks[i].Position))
				return true;
		}

		return false;
	}

	public void SetBackdrop(string name)
	{
		Material backdrop = BackdropFactory.GetBackdrop(name);
		cam.SetBackdrop(backdrop);
	}

	public void SetLight()
	{
		RenderSettings.ambientLight = new Color(1.0f, 1.0f, 1.0f);
	}

	public void SetDark()
	{
		RenderSettings.ambientLight = new Color(0.05f, 0.05f, 0.05f);
	}

	public void StartTileUpdates(Tile tile, int x, int y)
	{
		tileUpdates.Add(new TileInstance(tile, x, y));
	}

	public void StopTileUpdates(Tile tile, int x, int y)
	{
		tileUpdates.Remove(new TileInstance(tile, x, y));
	}

	private void Update()
	{
		if (Engine.IsPaused) return;

		for (int i = 0; i < tileUpdates.Count; i++)
		{
			TileInstance inst = tileUpdates[i];
			inst.tile.Update(this, inst.x, inst.y);
		}
			
		for (int i = 0; i < simulate.Count; i++)
			simulate[i].Simulate();
	}

	public void ChangeActiveChunk(Vector2i pos)
	{
		Chunk newChunk = GetChunk(pos);

		if (newChunk == null)
			return;
		
		SetChunkActive(newChunk, pos);
	}

	public void SetChunkActive(Vector2i pos)
	{
		SetChunkActive(chunks[pos.x, pos.y], pos);
	}

	public void SetChunkActive(Chunk center, Vector2i cPos)
	{
		for (int i = loadedChunks.Count - 1; i >= 0; i--)
		{
			Chunk chunk = loadedChunks[i];
			Vector2i pos = chunk.Position;

			int distX = Mathf.Abs(cPos.x - pos.x);
			int distY = Mathf.Abs(cPos.y - pos.y);

			if (distX > 1 || distY > 1)
			{
				chunk.Unload();
				loadedChunks.RemoveAt(i);
			}
		}

		for (int y = -1; y <= 1; y++)
		{
			for (int x = -1; x <= 1; x++)
			{
				Chunk chunk = GetChunk(new Vector2i(cPos.x + x, cPos.y + y));

				if (chunk == null || chunk.IsActive) continue;

				chunk.Load();
				loadedChunks.Add(chunk);
			}
		}

		for (int i = 0; i < notifyChunkChange.Count; i++)
			notifyChunkChange[i].CheckActivity();
	}

	private Vector3 FindValidSpawn(int x, int y)
	{
		while (y < 1024)
		{
			if (GetTile(x, y, 0).IsPassable() && GetTile(x, y + 1, 0).IsPassable())
				return new Vector3(x, y);

			y++;
		}

		Logger.LogError("Failed to find a spawn point.");
		return Vector3.zero;
	}

	public void SpawnPlayer(int x, int y)
	{
		Vector3 spawn = FindValidSpawn(x, y);
		Player player = SceneItems.GetItem<Player>("Player");
		player.Position = spawn;
		cam.Teleport(spawn);
		player.OnSpawn();
	}

	public void CreateEntity(string name, float x, float y)
	{
		Entity entity = EntityFactory.GetEntity(name);
		entity.Initialize(this, x, y, NextEntityID);

		Chunk chunk = GetChunk(Utils.WorldToChunkPos(entity.Position));

		if (chunk.IsActive) entity.MakeActive();
		else entity.MakeInactive();
	}

	private void ForceLoad(int genID)
	{
		nextLevel = GeneratorFromID(genID);
		BuildNextLevel();
	}

	public void BuildNextLevel()
	{
		if (!buildingLevel)
		{
			buildingLevel = true;
			Engine.Freeze();
			SceneItems.GetItem<ScreenFader>("Camera").GradualFade(1.0f, Color.black, BuildNextCallback);
		}
	}

	private void BuildNextCallback()
	{
		for (int y = 0; y < chunks.GetLength(1); y++)
		{
			for (int x = 0; x < chunks.GetLength(0); x++)
			{
				if (chunks[x, y] != null)
					chunks[x, y].Destroy();
			}
		}

		chunks = null;

		GameObject[] toDestroy = GameObject.FindGameObjectsWithTag("Disposable");

		for (int i = 0; i < toDestroy.Length; i++)
			Destroy(toDestroy[i]);

		Generate();
		System.GC.Collect();

		SceneItems.GetItem<ScreenFader>("Camera").GradualFade(1.0f, Color.clear, null);
		Engine.Unfreeze();
		buildingLevel = false;
	}

	public void Generate()
	{
		int seed = Random.Range(-5000, 5000);

		currentLevel = nextLevel;
		GetNextLevel();
		currentLevel.Generate(this, nextLevel, seed);
		levelCount++;
	}

	private void GetNextLevel()
	{
		int genID = 0;

		if (levelCount > 0)
		{
			genID = Random.Range(0, 2);

			if (genID == 1)
			{
				if (Random.value < 0.1f)
					genID = 2;
			}
		}

		nextLevel = GeneratorFromID(genID);
	}

	private Generator GeneratorFromID(int ID)
	{
		Generator generator;

		if (generators.TryGetValue(ID, out generator))
			return generator;

		return new VoidGenerator();
	}

	public bool InBounds(Vector2i pos)
	{
		return pos.x >= 0 && pos.y >= 0 && pos.x < chunks.GetLength(0) && pos.y < chunks.GetLength(1);
	}

	public bool InBounds(int x, int y)
	{
		return x >= 0 && y >= 0 && x < chunks.GetLength(0) && y < chunks.GetLength(1);
	}

	private void GameOver()
	{
		DataTransfer transfer = GameObject.FindWithTag("Carrier").GetComponent<DataTransfer>();
		transfer.score = score;
		transfer.coins = coins;
		SceneManager.LoadScene(Scenes.Lose);
	}
}

﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

public static class EditorTools
{
	[MenuItem("Tools/Play %l")]
	public static void Play()
	{
		if (!EditorApplication.isPlaying)
		{
			string scenePath = "Assets/Scenes/" + EditorSceneManager.GetActiveScene().name + ".unity";
			EditorPrefs.SetString("LastScene", scenePath);

			EditorSceneManager.OpenScene("Assets/Scenes/Loader.unity");
			EditorApplication.isPlaying = true;
		}
	}

	[MenuItem("Tools/Return %k")]
	public static void Return()
	{
		if (EditorPrefs.HasKey("LastScene"))
		{
			string scenePath = EditorPrefs.GetString("LastScene");
			EditorSceneManager.OpenScene(scenePath);
		}
	}

	[MenuItem("Tools/Open Save Folder")]
	private static void OpenSaveFolder()
	{
		EditorUtility.RevealInFinder(Application.persistentDataPath);
	}
}

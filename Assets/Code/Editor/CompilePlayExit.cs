﻿using UnityEngine;
using UnityEditor;

// Ensures play mode is exited when a recompile occurs, to prevent undesired behavior in the editor.

[InitializeOnLoad]
public sealed class CompilePlayExit 
{
	private static CompilePlayExit instance;

	static CompilePlayExit()
	{
		Unused(instance);
		instance = new CompilePlayExit();
	}

	private CompilePlayExit()
	{
		EditorApplication.update += OnEditorUpdate;
	}

	~CompilePlayExit()
	{
		EditorApplication.update -= OnEditorUpdate;
		instance = null;
	}

	private static void OnEditorUpdate() 
	{
		if (EditorApplication.isPlaying && EditorApplication.isCompiling) 
			EditorApplication.isPlaying = false;
	}

	private static void Unused<T>(T unusedVariable) {}
}
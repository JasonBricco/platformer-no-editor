﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public sealed class TextureBuilder : EditorWindow 
{
	private string lightName;
	private int radius;
	private float intensity;

	private string flatName;
	private Color flatColor;
	private int flatWidth, flatHeight;

	[MenuItem("Tools/Texture Builder")]
	public static void OpenLightBuilder()
	{
		EditorWindow.GetWindow(typeof(TextureBuilder), false, "Light Builder", true);
	}

	private void DrawDivider()
	{
		GUILayout.Box(String.Empty, GUILayout.ExpandWidth(true), GUILayout.Height(4));
	}

	private void OnGUI()
	{
		EditorGUILayout.LabelField("Light Texture");
		EditorGUILayout.Space();

		lightName = EditorGUILayout.TextField("Name", lightName);
		EditorGUILayout.Space();

		radius = EditorGUILayout.IntField("Radius (Tiles)", radius);
		EditorGUILayout.Space();

		intensity = EditorGUILayout.FloatField("Intensity (0-1)", intensity);
		EditorGUILayout.Space();

		if (GUILayout.Button("Generate"))
			BuildLight();

		EditorGUILayout.Space();
		DrawDivider();
		EditorGUILayout.Space();

		EditorGUILayout.LabelField("Flat Texture");
		EditorGUILayout.Space();

		flatName = EditorGUILayout.TextField("Name", flatName);
		EditorGUILayout.Space();

		flatWidth = EditorGUILayout.IntField("Width", flatWidth);
		flatHeight = EditorGUILayout.IntField("Height", flatHeight);
		EditorGUILayout.Space();

		flatColor = EditorGUILayout.ColorField("Color", flatColor);
		EditorGUILayout.Space();

		if (GUILayout.Button("Generate"))
			BuildFlat();
	}

	private void BuildLight()
	{
		radius *= (int)GameCamera.PixelsPerUnit;

		Texture2D tex = new Texture2D(radius * 2, radius * 2);
		Vector2i center = new Vector2i(tex.width / 2, tex.height / 2);

		for (int y = 0; y < tex.height; y++)
		{
			for (int x = 0; x < tex.width; x++)
			{
				int valueInCircle = Utils.Square(x - center.x) + Utils.Square(y - center.y);

				if (valueInCircle < Utils.Square(radius))
				{
					float dist = Vector2.Distance(new Vector2(x, y), center.ToVector2());
					float col = (1.0f - (dist / radius)) * intensity;
					col = Mathf.Pow(col, 2.0f);
				
					Color final = new Color(col, col, col, 1.0f);
					tex.SetPixel(x, y, final);
				}
				else tex.SetPixel(x, y, Color.black);
			}
		}
			
		string path = Application.dataPath + "/Textures/Lights/" + lightName + ".png";
		File.WriteAllBytes(path, tex.EncodeToPNG());
		AssetDatabase.Refresh();
		Close();
	}

	private void BuildFlat()
	{
		Texture2D tex = new Texture2D(flatWidth, flatHeight);

		for (int y = 0; y < tex.height; y++)
		{
			for (int x = 0; x < tex.width; x++)
				tex.SetPixel(x, y, flatColor);
		}

		string path = Application.dataPath + "/Textures/" + flatName + ".png";
		File.WriteAllBytes(path, tex.EncodeToPNG());
		AssetDatabase.Refresh();
		Close();
	}
}
	
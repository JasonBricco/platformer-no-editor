﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;

public static class LightFactory
{
	private static GameObject lightPrefab;

	public static void Load()
	{
		if (lightPrefab == null)
			lightPrefab = Resources.Load<GameObject>("Lights/Light");
	}

	public static GameObject GetLight(int diameter, Entity parent = null)
	{
		GameObject light = GameObject.Instantiate<GameObject>(lightPrefab);
		light.transform.localScale = new Vector3(diameter, diameter, 1.0f);

		if (parent != null) 
		{
			light.SetParent(parent.gameObject);
			parent.AssignLight(light);
		}
		
		return light;
	}
}

public static class BackdropFactory
{
	private static Material[] backdrops;
	private static Dictionary<string, int> mapping;

	public static void Load()
	{
		backdrops = Resources.LoadAll<Material>("Backdrops");
		mapping = new Dictionary<string, int>(backdrops.Length);

		for (int i = 0; i < backdrops.Length; i++)
			mapping.Add(backdrops[i].name, i);
	}

	public static Material GetBackdrop(string name)
	{
		if (name == null) return null;

		Assert.IsTrue(mapping.ContainsKey(name), "No backdrop exists with the name: " + name + ".");
		return backdrops[mapping[name]];
	}
}

public static class EntityFactory
{
	private static GameObject[] entities;
	private static Dictionary<string, int> mapping;

	public static void Load()
	{
		entities = Resources.LoadAll<GameObject>("Entities");
		mapping = new Dictionary<string, int>(entities.Length);

		for (int i = 0; i < entities.Length; i++)
			mapping.Add(entities[i].name, i);
	}

	public static Entity GetEntity(string name)
	{
		Assert.IsTrue(mapping.ContainsKey(name), "No entity exists with the name: " + name + ".");

		GameObject obj = GameObject.Instantiate<GameObject>(entities[mapping[name]]);
		obj.name = name;

		return obj.GetComponent<Entity>();
	}
}

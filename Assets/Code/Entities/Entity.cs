﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Assertions;
using System;

public enum EntityActivity
{
	Active,
	Inactive
}

public enum EntityState
{
	Idle,
	Walking,
	Falling
}

public class Entity : MonoBehaviour, IEquatable<Entity>
{
	private int ID;

	protected Map map;

	protected Transform t;
	protected SpriteRenderer rend;
	protected Animator animator;
	protected Collider col;

	protected EntityActivity activity = EntityActivity.Inactive;
	protected EntityState animationState = EntityState.Idle;

	protected const int MaxHealth = 20;
	protected int health = MaxHealth;

	protected const float Gravity = -30.0f;
	protected float friction = -8.0f;
	protected float speed = 50.0f;

	protected float immuneLength = 0.0f;
	protected float immuneRemaining = 0.0f;

	protected Vector2i chunkPos;
	protected Vector2 velocity;

	protected CharacterController controller;

	protected int inactiveWidth = 1;
	protected bool isDead = false;

	protected Material mat;

	public Vector3 Position
	{
		get { return t.position; }
		set { t.position = value; }
	}
		
	public virtual void Initialize(Map map, float x, float y, int ID)
	{
		this.ID = ID;
		this.map = map;

		CacheComponents();
		t.position = new Vector3(x, y, GetLayer());
		chunkPos = Utils.WorldToChunkPos(Position);
		mat = rend.material;
	}

	public virtual void Simulate() {}
	public virtual void CheckActivity() {}

	public bool Equals(Entity other)
	{
		return this.ID == other.ID;
	}

	protected virtual float GetLayer()
	{
		return 0.0f;
	}

	public void SetVelocityX(float x)
	{
		velocity.x = x;
	}

	public void SetVelocityY(float y)
	{
		velocity.y = y;
	}

	public virtual void CacheComponents()
	{
		t = GetComponent<Transform>();
		animator = GetComponent<Animator>();
		rend = GetComponent<SpriteRenderer>();
		col = GetComponent<Collider>();
		controller = col as CharacterController;
	}

	public virtual void MakeActive()
	{
		if (activity != EntityActivity.Active)
		{
			ToggleComponents(true);
			activity = EntityActivity.Active;
		}
	}

	public virtual void MakeInactive()
	{
		if (activity != EntityActivity.Inactive)
		{
			ToggleComponents(false);
			activity = EntityActivity.Inactive;
		}
	}

	protected virtual void ToggleComponents(bool state) 
	{
		animator.enabled = state;
		col.enabled = state;
		rend.enabled = state;
	}
		
	protected bool CheckEnvironment(Vector2i tilePos)
	{
		Vector2i bounds = map.GetSize();
		Vector3 newPos = t.position;

		if (Utils.TryClamp(ref newPos.x, 0.0f, bounds.x * Chunk.SizeX))
			RespondToEdge();

		t.position = newPos;

		for (int y = 0; y < 2; y++)
		{
			Vector2i pos = new Vector2i(tilePos.x, tilePos.y + y);
			map.GetTile(pos, 0).OnEnter(map, this, pos.x, pos.y);
		}

		Vector2i offset = new Vector2i(0, 0);

		Vector2i min = Utils.TileFromChunkCorner(chunkPos);
		Vector2 local = new Vector2(t.position.x - min.x, t.position.y - min.y);

		offset.x += Mathf.FloorToInt(local.x / Chunk.SizeX);
		offset.y += Mathf.FloorToInt(local.y / Chunk.SizeY);

		if (offset.x != 0 || offset.y != 0)
		{
			chunkPos += offset;
			return true;
		}

		return false;
	}

	protected virtual void RespondToEdge() {}

	public virtual void TakeDamage(int damage, Vector3 dir)
	{
		if (Mathf.Approximately(immuneRemaining, 0.0f))
		{
			immuneRemaining = immuneLength;
			health -= damage;

			SetVelocityX(dir.x * 25.0f);

			mat.color = Color.red;
			Invoke("EndFlash", 0.1f);
		}
	}
		
	public virtual void AssignLight(GameObject light) {}

	private void EndFlash()
	{
		mat.color = Color.white;
	}

	protected virtual void Kill(bool addScore)
	{
		GameObject.Destroy(gameObject, 0.15f);
	}
}

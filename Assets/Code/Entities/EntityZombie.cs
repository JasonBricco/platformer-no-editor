﻿using UnityEngine;

public sealed class EntityZombie : EntityPatrol 
{
	public override void Initialize(Map map, float x, float y, int ID)
	{
		base.Initialize(map, x, y, ID);

		speed = 15.0f;
		inactiveWidth = 2;
		immuneLength = 0.2f;
		score = 100;
	}
}

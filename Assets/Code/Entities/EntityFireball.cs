﻿using UnityEngine;
using System.Collections.Generic;

public sealed class EntityFireball : Entity 
{
	private Vector3 direction;

	private float energy = 5.0f;

	private Queue<EntityFireball> pool;

	private GameObject fireLight;

	public void Initialize(Map map, float x, float y, int ID, Queue<EntityFireball> pool)
	{
		base.Initialize(map, x, y, ID);

		this.pool = pool;
		this.speed = 15.0f;

		LightFactory.GetLight(6, this);
	}

	public override void MakeActive()
	{
		base.MakeActive();
		fireLight.SetActive(true);
		energy = 5.0f;
		map.BeginSimulating(this);
	}

	public override void MakeInactive()
	{
		base.MakeInactive();
		fireLight.SetActive(false);
		map.EndSimulating(this);
	}

	public void SetPath(Vector3 start, Vector3 dir)
	{
		t.position = start;
		direction = new Vector3(dir.x, dir.y, 0.0f);
	}

	public override void Simulate()
	{
		t.Translate(direction * speed * Time.deltaTime);

		energy -= Time.deltaTime;

		if (energy <= 0.0f)
			Kill();
	}

	private void OnTriggerEnter(Collider other)
	{
		int layer = other.gameObject.layer;

		switch (layer)
		{
		case CollisionLayer.Terrain:
			Kill();
			break;

		case CollisionLayer.Enemy:
			other.GetComponentInParent<Entity>().TakeDamage(5, Vector3.zero);
			Kill();
			break;
		}
	}

	public override void AssignLight(GameObject light)
	{
		this.fireLight = light;
	}

	protected override void ToggleComponents(bool state)
	{
		col.enabled = state;
		rend.enabled = state;
	}

	private void Kill()
	{
		MakeInactive();
		pool.Enqueue(this);
	}

	private void OnDestroy()
	{
		map.EndSimulating(this);
	}
}

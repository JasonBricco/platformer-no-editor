﻿using UnityEngine;

public class EntityPatrol : Entity 
{
	protected int score;
	protected CapsuleCollider trigger;

	private Vector3 direction = Vector3.left;

	private bool doJump = false;

	public override void Initialize(Map map, float x, float y, int ID)
	{
		map.NotifyForChunkChange(this);
		base.Initialize(map, x, y, ID);
	}

	public override void CheckActivity()
	{
		bool active = map.IsChunkActive(chunkPos);

		switch (activity)
		{
		case EntityActivity.Active:
			if (!active) MakeInactive();
			break;

		case EntityActivity.Inactive:
			if (active) MakeActive();
			break;
		}
	}

	public override void MakeActive()
	{
		if (activity != EntityActivity.Active)
		{
			base.MakeActive();
			map.BeginSimulating(this);
		}
	}

	public override void MakeInactive()
	{
		if (activity != EntityActivity.Inactive)
		{
			base.MakeInactive();
			map.EndSimulating(this);
		}
	}

	protected override void RespondToEdge()
	{
		FlipDirection();
	}

	private void CheckForFall(int xOffset)
	{
		Vector2i tPos = Utils.WorldToTilePos(t.position);

		int testX = tPos.x + xOffset, testY = tPos.y - 1;

		if (map.GetTile(testX, testY, 0).IsPassable())
		{
			for (int i = 1; i <= 5; i++)
			{
				if (!map.GetTile(testX, testY - i, 0).IsPassable())
					return;
			}

			FlipDirection();
		}
	}

	public override void Simulate()
	{
		if (activity == EntityActivity.Inactive) return;

		immuneRemaining = Mathf.Max(immuneRemaining - Time.deltaTime, 0.0f);

		if (controller.isGrounded) velocity.y = 0.0f;

		float f = Utils.Frac(t.position.x);

		if (f < 0.1f && direction == Vector3.left)
			CheckForFall(-1);
		else if (f > 0.9f && direction == Vector3.right)
			CheckForFall(1);

		Vector2 accel = direction;

		if (doJump)
		{
			velocity.y = 5.0f;
			doJump = false;
		}
		
		accel *= speed;
		accel += velocity * friction;
		accel.y = Gravity;

		Vector2 delta = (accel * 0.5f * Mathf.Pow(Time.deltaTime, 2.0f)) + (velocity * Time.deltaTime);
		velocity = (accel * Time.deltaTime) + velocity;

		rend.flipX = delta.x > 0.0f;

		if (!controller.isGrounded) 
			animationState = EntityState.Falling;
		else animationState = Mathf.Abs(delta.x) < 0.005f ? EntityState.Idle : EntityState.Walking;

		PlayAnimation();

		CollisionFlags flags = controller.Move(delta);

		if ((flags & CollisionFlags.Sides) != 0)
		{
			Vector2i tPos = Utils.WorldToTilePos(Position);
			Vector2i start = new Vector2i((int)(tPos.x + direction.x), tPos.y + 1);

			if (map.PassableRegion(start.x, start.y, start.x, start.y + 1))
				doJump = true;
			else FlipDirection();
		}

		ValidatePosition();
	}

	private void PlayAnimation()
	{
		switch (animationState)
		{
		case EntityState.Idle:
		case EntityState.Falling:
			animator.Play(AnimationStates.Idle);
			break;

		case EntityState.Walking:
			animator.Play(AnimationStates.Walk);
			break;
		}
	}

	private void FlipDirection()
	{
		velocity = Vector2.zero;

		if (direction == Vector3.left) direction = Vector3.right;
		else direction = Vector3.left;
	}

	public override void CacheComponents()
	{
		base.CacheComponents();
		trigger = GetComponentInChildren<CapsuleCollider>(true);
	}

	private void ValidatePosition()
	{
		if (CheckEnvironment(Utils.WorldToTilePos(Position)))
		{
			if (Position.y < 0.0f)
			{
				Kill(false);
				return;
			}

			switch (activity)
			{
			case EntityActivity.Active:
				if (map.InBounds(chunkPos) && !map.IsChunkActive(chunkPos))
				{
					Vector2i local = Utils.WorldToLocalPos(t.position);

					if (!map.PassableRegion(local.x, local.y, local.x, local.y + 1))
						FlipDirection();
					else MakeInactive();
				}
				break;

			case EntityActivity.Inactive:
				if (map.IsChunkActive(chunkPos))
					MakeActive();
				break;
			}
		}
	}

	protected override void ToggleComponents(bool state)
	{
		base.ToggleComponents(state);
		trigger.enabled = state;
	}

	public override void TakeDamage(int damage, Vector3 dir)
	{
		// Check if the enemy is already dead, to prevent further attempts at killing it before it gets destroyed.
		if (isDead) return;

		base.TakeDamage(damage, dir);

		if (health <= 0)
			Kill(true);
	}

	protected override void Kill(bool addScore)
	{
		isDead = true;

		if (addScore) 
			map.AddScore(score);
		
		base.Kill(addScore);
	}

	private void OnDestroy()
	{
		map.EndSimulating(this);
		map.EndNotifyForChunkChange(this);
	}
}

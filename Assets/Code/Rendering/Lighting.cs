﻿using UnityEngine;
using UnityEngine.Assertions;

public sealed class Lighting : MonoBehaviour 
{
	private Material material;
	private Camera scanner;

	private void Awake()
	{
		material = new Material(Shader.Find("Custom/Lighting"));
		scanner = gameObject.FindChild("Scanner").GetComponent<Camera>();

		RenderTexture lightmap = new RenderTexture(Screen.width, Screen.height, 0);
		scanner.targetTexture = lightmap;

		material.SetTexture("_Lightmap", lightmap);
	}
		
	private void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		Graphics.Blit(source, destination, material);
	}
}

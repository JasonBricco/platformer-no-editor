﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class GameCamera : MonoBehaviour
{
	public const float PixelsPerUnit = 32.0f;
	public const int PPUShift = 5;

	private Camera cam;
	private Transform t;
	private Transform target;

	private Vector3 velocity;
	private float dampTime = 0.25f;

	private float minX, maxX, minY, maxY;
	private Vector3 lastPos;

	private bool hasBackdrop = false;
	private MeshRenderer backdrop;

	private Vector2 uvOffset;

	private void Start()
	{
		cam = GetComponent<Camera>();
		t = GetComponent<Transform>();
		backdrop = gameObject.FindChild("Backdrop").GetComponent<MeshRenderer>();

		switch (Engine.Device)
		{
		case DeviceType.PC:
			if (Screen.width != 1024 || Screen.height != 512)
				Screen.SetResolution(1024, 512, false);
			cam.orthographicSize = 8;
			backdrop.transform.localScale = new Vector3(32.0f, 16.0f);
			break;

		case DeviceType.iPhone:
			cam.orthographicSize = 9;
			backdrop.transform.localScale = new Vector3(32.0f, 18.0f);
			break;

		case DeviceType.iPad:
			cam.orthographicSize = 12;
			backdrop.transform.localScale = new Vector3(32.0f, 24.0f);
			break;
		}

		target = SceneItems.GetItem<Player>("Player").transform;
	}

	public void SetBoundaries(int chunkWidth, int chunkHeight)
	{
		minX = Chunk.SizeX / 2.0f;
		maxX = (chunkWidth * Chunk.SizeX) - minX;
		minY = cam.orthographicSize;
		maxY = Mathf.Max((chunkHeight * Chunk.SizeY) - minY, minY);
	}

	public void Teleport(Vector3 pos)
	{
		t.position = pos;
	}

	private void LateUpdate()
	{
		if (Engine.IsPaused) return;

		Vector3 vP = Camera.main.WorldToViewportPoint(target.position);
		Vector3 delta = target.position - Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, vP.z));
		Vector3 end = t.position + delta;

		Vector3 newPos = Vector3.SmoothDamp(t.position, end, ref velocity, dampTime);
		newPos.x = Mathf.Clamp(newPos.x, minX, maxX);
		newPos.y = Mathf.Clamp(newPos.y, minY, maxY);
	
		float scaledX = Mathf.Round(PixelsPerUnit * newPos.x);
		float scaledY = Mathf.Round(PixelsPerUnit * newPos.y);

		newPos = new Vector3(scaledX / PixelsPerUnit, scaledY / PixelsPerUnit, -10.0f);

		if (hasBackdrop) ScrollBackground(newPos - lastPos);

		t.position = newPos;
		lastPos = newPos;
	}

	private void ScrollBackground(Vector3 delta)
	{
		Vector2 move = (Vector2)(delta * 0.005f);
		uvOffset += move;

		uvOffset.x %= 1.0f;
		uvOffset.y %= 1.0f;

		backdrop.sharedMaterial.SetTextureOffset("_MainTex", uvOffset);
	}

	public string GetBackdropName()
	{
		return backdrop.sharedMaterial.name;
	}

	public void SetBackdrop(Material material)
	{
		hasBackdrop = material != null;

		if (hasBackdrop)
		{
			backdrop.enabled = true;
			backdrop.sharedMaterial = material;

			switch (Engine.Device)
			{
			case DeviceType.PC:
				material.mainTextureScale = new Vector2(2.0f, 1.0f);
				break;

			case DeviceType.iPhone:
				material.mainTextureScale = new Vector2(2.0f, 1.12f);
				break;

			case DeviceType.iPad:
				material.mainTextureScale = new Vector2(2.0f, 1.333f);
				break;
			}
		}
		else backdrop.enabled = false;
	}
}

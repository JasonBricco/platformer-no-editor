﻿using UnityEngine;
using System;
using System.Collections.Generic;

public sealed class Chunk : IEquatable<Chunk>
{
	public const int MaxLayers = 4;
	public const int SizeX = 32, SizeY = 16;

	public const int ShiftX = 5, ShiftY = 4;
	public const int MaskX = SizeX - 1, MaskY = SizeY - 1;

	public const byte MinFluid = 1, MaxFluid = 8;

	private List<Tile[]> tiles = new List<Tile[]>(MaxLayers);

	private CollisionManager collisionManager;
	private Queue<BoxCollider> colliders = new Queue<BoxCollider>();

	private Map map;
	private bool simulatingFluid;

	private bool flagged;

	public bool IsActive { get; private set; }
	public Vector2i Position { get; private set; }

	public Chunk(int x, int y, Map map)
	{
		Position = new Vector2i(x, y);

		this.map = map;

		for (int i = 0; i < MaxLayers; i++)
			tiles.Add(null);

		CreateLayer(0);

		collisionManager = SceneItems.GetItem<CollisionManager>("Collision");
	}

	public bool Equals(Chunk other)
	{
		return Position.x == other.Position.x && Position.y == other.Position.y;
	}

	public void FlagForUpdate(Queue<Chunk> updateQueue)
	{
		if (!flagged)
		{
			updateQueue.Enqueue(this);
			flagged = true;
		}
	}

	public void CreateLayer(int layer)
	{
		tiles[layer] = new Tile[SizeX * SizeY];
		Fill(Tile.Air, layer);
	}

	public Tile GetTile(int x, int y, int layer)
	{
		if (tiles.Count > layer)
			return tiles[layer][y * SizeX + x];

		return Tile.Air;
	}

	public Tile GetTile(Vector2i pos, int layer)
	{
		return GetTile(pos.x, pos.y, layer);
	}
		
	public Tile GetTile(int index, int layer)
	{
		if (tiles[layer] != null)
			return tiles[layer][index];

		return Tile.Air;
	}
		
	public Tile GetTileSafe(int x, int y, int layer)
	{
		if (!InBounds(x, y))
		{
			Vector2i tilePos = Utils.LocalToTilePos(x, y, Position);
			return map.GetTile(tilePos, layer);
		}

		return GetTile(x, y, layer);
	}

	public void SetTile(int x, int y, Tile tile, int layer)
	{
		Tile[] target = tiles[layer];

		if (target == null)
		{
			CreateLayer(layer);
			target = tiles[layer];
		}

		int index = y * SizeX + x;

		Vector2i tPos = Utils.LocalToTilePos(x, y, Position);
		Tile prevTile = target[index];

		if (!prevTile.Equals(tile.ID))
		{
			target[index] = tile;
			prevTile.OnDelete(this, x, y);

			if (prevTile.IsActive())
			{
				prevTile.Deactivate(map, tPos);
				tile.Activate(map, tPos, true);
			}
				
			tile.OnSet(this, x, y);
		}
	}

	public void SetTileSafe(int x, int y, int layer, Tile tile)
	{
		if (!InBounds(x, y))
		{
			Vector2i tPos = Utils.LocalToTilePos(x, y, Position);
			map.SetTile(tPos.x, tPos.y, tile, layer);
		}
		else SetTile(x, y, tile, layer);
	}

	public void SetRect(int startX, int startY, int endX, int endY, Tile tile, int layer)
	{
		for (int y = startY; y <= endY; y++)
		{
			for (int x = startX; x <= endX; x++)
				SetTile(x, y, tile.Clone(), layer);
		}
	}

	public void Fill(Tile tile, int layer)
	{
		Tile[] target = tiles[layer];

		if (target == null)
		{
			tiles[layer] = new Tile[SizeX * SizeY];
			target = tiles[layer];
		}

		for (int i = 0; i < target.Length; i++)
			target[i] = tile.Clone();
	}

	public void Load()
	{
		for (int layer = 0; layer < tiles.Count; layer++)
		{
			Tile[] target = tiles[layer];

			if (target != null)
			{
				for (int y = 0; y < SizeY; y++)
				{
					for (int x = 0; x < SizeX; x++)
					{
						Vector2i tPos = Utils.LocalToTilePos(x, y, Position);
						target[y * SizeX + x].Activate(map, tPos, true);
					}
				} 
			}
		}
			
		collisionManager.GenerateGreedy(this, colliders);
		IsActive = true;
	}

	public void Unload()
	{
		for (int layer = 0; layer < tiles.Count; layer++)
		{
			Tile[] target = tiles[layer];

			if (target != null)
			{
				for (int y = 0; y < SizeY; y++)
				{
					for (int x = 0; x < SizeX; x++)
					{
						Tile tile = target[y * SizeX + x];

						if (tile.IsActive())
							tile.Deactivate(map, Utils.LocalToTilePos(x, y, Position));
					}
				} 
			}
		}

		collisionManager.ReturnColliders(colliders);
		IsActive = false;
	}

	public void Destroy()
	{
		Unload();

		for (int i = 0; i < MaxLayers; i++)
			tiles[i] = null;
	}

	public static bool InBounds(int x, int y)
	{
		return x >= 0 && y >= 0 && x < SizeX && y < SizeY;
	}
}

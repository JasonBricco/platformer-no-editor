﻿using UnityEngine;
using UnityEngine.SceneManagement;

public sealed class DataTransfer : MonoBehaviour
{
	[HideInInspector] public int score;
	[HideInInspector] public int coins;

	private void Awake()
	{
		DontDestroyOnLoad(this);
		SceneManager.LoadScene(Scenes.MainMenu);
	}
}

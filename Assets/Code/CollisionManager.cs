﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class CollisionManager : MonoBehaviour
{
	private const int XLimit = Chunk.SizeX;
	private const int YLimit = Chunk.SizeY;

	private Queue<BoxCollider> pool = new Queue<BoxCollider>(64);

	private BitArray mask;

	private void Awake()
	{
		mask = new BitArray(XLimit * YLimit);

		for (int i = 0; i < Chunk.SizeY; i++)
		{
			BoxCollider col = CreateCollider();
			col.enabled = false;
			pool.Enqueue(col);
		}
	}

	private BoxCollider CreateCollider()
	{
		GameObject obj = new GameObject("Collider");
		obj.layer = 10;
		Rigidbody rb = obj.AddComponent<Rigidbody>();
		rb.isKinematic = true;

		return obj.AddComponent<BoxCollider>();
	}

	private BoxCollider GetCollider(Queue<BoxCollider> colliders)
	{
		BoxCollider col;

		if (pool.Count > 0) 
		{
			col = pool.Dequeue();
			col.enabled = true;
		}
		else col = CreateCollider();

		colliders.Enqueue(col);
		return col;
	}

	public void ReturnColliders(Queue<BoxCollider> colliders)
	{
		while (colliders.Count > 0)
		{
			BoxCollider collider = colliders.Dequeue();
			collider.size = Vector3.one;
			collider.enabled = false;
			pool.Enqueue(collider);
		}
	}
		
	public void GenerateGreedy(Chunk chunk, Queue<BoxCollider> colliders)
	{
		Vector2i min = Utils.TileFromChunkCorner(chunk.Position);

		for (int i = 0; i < mask.Count; i++)
			mask.Set(i, !chunk.GetTile(i, 0).IsPassable());

		int x = 0, y = 0;
		int startX = x;
		int width = 1, height = 1;
		bool building = false;
		BoxCollider current = null;

		while (true)
		{
			int index = y * XLimit + x;
			bool shouldCollide = mask.Get(index);

			if (shouldCollide)
			{
				if (!building)
				{
					building = true;
					startX = x;
					current = GetCollider(colliders);
					current.transform.position = new Vector3(min.x + x, min.y + y);
				}
				else
					width++;
			}

			if (building && ((x == XLimit - 1) || !shouldCollide))
			{
				int row = y + 1;
				bool valid = true;

				while (true)
				{
					if (row == YLimit) break;

					for (int i = startX; i < startX + width; i++)
					{
						if (!mask.Get(row * XLimit + i))
						{
							valid = false;
							break;
						}
					}

					if (valid)
					{
						for (int i = startX; i < startX + width; i++)
							mask.Set(row * XLimit + i, false);

						height++;
						row++;
					}
					else break;
				}

				current.size = new Vector3(width, height, 1.0f);
				current.center = new Vector3(width * 0.5f, height * 0.5f);
				width = 1;
				height = 1;
				building = false;
			}

			mask.Set(index, false);
			x++;

			if (x == XLimit)
			{
				x = 0;
				width = 1;
				y++;

				if (y == YLimit) break;
			}
		}
	}
}
